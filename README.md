# README #

Setting up a windows system for command line development.




Software
========

Git
---
Download from http://msysgit.github.io/ and install to T:\apps\git

> Check "Use Git from the Windows Command Prompt"

> Check "Checkout Windows-style, commit Unix-style line endings"

Node.js
-------
Download from http://nodejs.org/ and install to Path T:\apps\nodejs

ConEmu
------
The console for windows that doesn't suck. Download from http://www.fosshub.com/ConEmu.html and install to T:\apps\ConEmu
	
> Settings > Startup > Tasks > "+" button

> - Group: Git Bash

> - Commands: "T:\apps\git\bin\sh.exe" --login -i

> Settings > Integration > ConEmu Here

> - Menu Item: ConEmu Here (Git Bash)

> - Command: /icon "T:\apps\git\etc\git.ico"  /single /cmd {Git Bash}

> - Icon file: T:\apps\git\etc\git.ico

> - Click the Register button

Download nano from http://www.nano-editor.org/download.php and unpack it to T:\apps\nano so that vim can be replaced in git-bash. Create an empty file in T:\apps\git\bin that is called nano with no extentions.

> &#35!/bin/sh

> exec T:/apps/nano/nano.exe "$@"

Download clink portable from http://mridgers.github.io/clink/ and unpack it to T:\apps\ConEmu\ConEmu\clink so that the default cmd console supports things like Ctrl+P paste and bin like typeahead. For a list of clink added capabilities view the documentation at https://github.com/mridgers/clink/blob/master/docs/clink.md

> ConEmu > Settings > Features > Check "Use Clink in prompt"



SSH Keys
========
It is recommended to use a passphase

    ssh-keygen -t rsa
    eval "$(ssh-agent -s)"
    ssh-add ~/.ssh/id_rsa
    clip < ~/.ssh/id_rsa.pub

Add to GitHub
-------------
https://github.com/settings/ssh

    ssh -T git@github.com


Add to BitBucket
----------------
https://bitbucket.org/account/user/targilnar/ssh-keys/

    ssh -T git@bitbucket.org

Test Auto-load SSH Agent
------------------------
Restart your bash shell (after copying over configuration for bash profile). You should be prompted for your pass phrase. Run both SSH tests to confirm that the agent is running holding your key.