# Force the bashrc to load when using SSH to connect remotely
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi