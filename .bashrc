##############################
##### SYSTEM & OS
##############################
function parse_git_branch {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo "("${ref#refs/heads/}")"
}

# Default to human readable figures
alias du='du -h'
# alias df='df -h' 								  #(non-windows)

# showa: remind myself about aliases
# showa () { /usr/bin/grep -i -a1 $@ ~/.aliases.bash | grep -v '^\s*$' ; }

# sourcea: refresh aliases file
# alias sourcea='source ~/.aliases.bash'

RED="\[\033[0;31m\]"
YELLOW="\[\033[1;33m\]"
GREEN="\[\033[1;32m\]"
WHITE="\[\033[1;37m\]"

# PS1="$WHITE\$(date +%H:%M) \w$YELLOW \$(parse_git_branch)$WHITE\$ "

# Enable color support
alias l='ls -a --color=auto'
alias ll='ls -la --color=auto'				      # long list
alias ls='ls -hF --color=tty'                 	  # classify files in colour
# alias grep='grep --color'                         # show differences in colour (non-windows)
# alias egrep='egrep --color=auto'                  # show differences in colour (non-windows)
# alias fgrep='fgrep --color=auto'                  # show differences in colour (non-windows)

# zipf: to create a ZIP archive of a folder
zipf () { zip -r "$1".zip "$1" ; }


##############################
##### SEARCHING
##############################

# ff:  to find a file under the current directory
ff () { /usr/bin/find . -name "$@" ; }

# ffs: to find a file whose name starts with a given string
ffs () { /usr/bin/find . -name "$@"'*' ; }

# ffe: to find a file whose name ends with a given string
ffe () { /usr/bin/find . -name '*'"$@" ; }

##############################
##### SCM (git/svn)
##############################
alias gc='git checkout'                           #check out repo
alias gist='git status'                           #current status of the git repo
alias gpl='git pull'
alias gpu='git push'
alias gcm='git commit -v -m'                      #commit with custom message and diff in message
alias gca='git commit -v -a -m'                   #all of the above commits but also opens to view
alias gall='git add .'                            #add modified files to be committed
alias ginfo='$HOME/git-info.sh'					  #displays info about current git repo
alias gitinfo='$HOME/git-info.sh'				  #displays info about current git repo


##############################
##### SSH Key Handling
##############################
# Note: ~/.ssh/environment should not be used, as it
#       already has a different purpose in SSH.

env=~/.ssh/agent.env

# Note: Don't bother checking SSH_AGENT_PID. It's not used
#       by SSH itself, and it might even be incorrect
#       (for example, when using agent-forwarding over SSH).

agent_is_running() {
    if [ "$SSH_AUTH_SOCK" ]; then
        # ssh-add returns:
        #   0 = agent running, has keys
        #   1 = agent running, no keys
        #   2 = agent not running
        # if your keys are not stored in ~/.ssh/id_rsa.pub or ~/.ssh/id_dsa.pub, you'll need
        # to paste the proper path after ssh-add
        ssh-add -l >/dev/null 2>&1 || [ $? -eq 1 ]
    else
        false
    fi
}

agent_has_keys() {
    # if your keys are not stored in ~/.ssh/id_rsa.pub or ~/.ssh/id_dsa.pub, you'll need
    # to paste the proper path after ssh-add
    ssh-add -l >/dev/null 2>&1
}

agent_load_env() {
    . "$env" >/dev/null
}

agent_start() {
    (umask 077; ssh-agent >"$env")
    . "$env" >/dev/null
}

if ! agent_is_running; then
    agent_load_env
fi

# if your keys are not stored in ~/.ssh/id_rsa.pub or ~/.ssh/id_dsa.pub, you'll need
# to paste the proper path after ssh-add
if ! agent_is_running; then
    agent_start
    ssh-add
elif ! agent_has_keys; then
    ssh-add
fi

unset env